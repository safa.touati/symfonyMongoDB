<?php
/**
 * Created by PhpStorm.
 * User: safa
 * Date: 17/08/2018
 * Time: 10:14 AM
 */

namespace AppBundle\Controller;



use DateTime;
use AppBundle\Document\Formulaire;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use FOS\RestBundle\Controller\FOSRestController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use FOS\RestBundle\Controller\Annotations\View;
use AppBundle\Document\Repository\UtilisateurRepository;

class FormulaireController extends Controller
{

    /**
     * @Rest\Post(
     *   path = "form/{id}"
     * )
     */
   public function createFormAction(Request $request, $id)
    {
        $user = $this->get('doctrine_mongodb')
            ->getRepository('AppBundle:User')
            ->find($id);

        if (!$user) {
            throw $this->createNotFoundException('No user found for id '.$id);
        }


        $formulaire = new Formulaire();
        $formulaire->setTitle($request->request->get('title'));
        $formulaire->setFormDescription($request->request->get('formDescription'));
        $formulaire->setCreationDate($request->request->get('creationDate'));
        $formulaire->setLastModifDate($request->request->get('lastModifDate'));
        $formulaire->setUser($user);

        $dm = $this->get('doctrine_mongodb')->getManager();
        $dm->persist($formulaire);
        $dm->flush();

        return new Response('Formulaire ID:'.json_encode($formulaire->getId()));
    }
//modifier
    /**
     * @Rest\Post(
     *   path = "form/update/{id}"
     * )
     */
    public function updateFormulaireUser(Request $request, $id)
    {
        $formulaire = $this->get('doctrine_mongodb')
            ->getRepository('AppBundle:Formulaire')
            ->find($id);

        if (!$formulaire) {
            throw $this->createNotFoundException('No Forms found ');
        }

        $formulaire->setTitle($request->request->get('title'));
        $formulaire->setFormDescription($request->request->get('formDescription'));
        $formulaire->setCreationDate($request->request->get('creationDate'));
        $formulaire->setLastModifDate($request->request->get('lastModifDate'));

        $dm = $this->get('doctrine_mongodb')->getManager();
        $dm->persist($formulaire);
        $dm->flush();

        return new Response('Formulaire ID:'.json_encode($formulaire->getId()));
    }

//delete
    /**
     * @Rest\Delete(
     *   path = "form/delete/{id}"
     * )
     */
    public function deleteForm($id)
    {
        $formulaire = $this->get('doctrine_mongodb')
            ->getRepository('AppBundle:Formulaire')
            ->find($id);

        if (!$formulaire) {
            throw $this->createNotFoundException('No Forms found ');
        }
        $dm = $this->get('doctrine_mongodb')->getManager();
        $dm->remove($formulaire);
        $dm->flush();

        return new Response('Formulaire ID:'.$id);
    }

    /**
     * @Rest\Get(
     *   path = "form/liste/{id}"
     * )
     */
    public function getAllFormsAction($id)
    {
        $user = $this->get('doctrine_mongodb')
            ->getRepository('AppBundle:User')
            ->find($id);

        if (!$user) {
            throw $this->createNotFoundException('No user found for id '.$id);
        }

        $i = 0;
        $result = [];
        $formulaires = $user->getFormulaires();
        foreach ($formulaires as $formulaire) {
            $result[$i]['title'] = (string)$formulaire->getTitle();
            $result[$i]['formDescription'] = (string)$formulaire->getFormDescription();
            $result[$i]['creationDate'] = $formulaire->getCreationDate()->format(DateTime::ISO8601);;
            $result[$i]['lastModifDate'] = $formulaire->getLastModifDate()->format(DateTime::ISO8601);;

            $i++;
        }

        return new Response(json_encode($result));
    }


    /**
     * @Rest\Get(
     *   path = "form/one/{id}"
     * )
     */

    public function getOneFormAction($id)
    {
        $formulaire = $this->get('doctrine_mongodb')
            ->getRepository('AppBundle:Formulaire')
            ->find($id);

        if (!$formulaire) {
            throw $this->createNotFoundException('No Forms found ');
        }

        $result['title'] = (string)$formulaire->getTitle();
        $result['formDescription'] = (string)$formulaire->getFormDescription();
        $result['creationDate'] = $formulaire->getCreationDate()->format(DateTime::ISO8601);;
        $result['lastModifDate'] = $formulaire->getLastModifDate()->format(DateTime::ISO8601);;

        return new Response(json_encode($result));
    }
}