<?php

namespace AppBundle\Controller;
use AppBundle\Document\Utilisateur;
use FOS\RestBundle\Controller\Annotations as Rest;
use AppBundle\Document\Repository\ProductRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use AppBundle\Document\Product;
use AppBundle\Document\Article;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('default/index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')).DIRECTORY_SEPARATOR,
        ]);
    }

    /**
     * @Route("/createA", name="createArticle")
     */
   /* public function createAAction()
    {
    $product = new Article();
        $product->setTitle('harry');
         $product->setContent('hello word');


         $dm = $this->get('doctrine_mongodb')->getManager();
         $dm->persist($product);
         $dm->flush();

         return new Response('Created product id '.$product->getId());

    }*/




    /**
     * @Route("/create", name="createProduct")
     */
    public function createAction()
    {
       /*$product = new Article();
        $product->setContent('A Foo Bar');
        $product->setTitle('19.99');

        $dm = $this->get('doctrine_mongodb')->getManager();
        $dm->persist($product);
        $dm->flush();

        return new Response('Created product id '.$product->getId());*/
        $user = new Utilisateur();
        $user->setUsername('A Foo Bar');
        $user->setPassword('19.99');
        $user->setEmail('A Foo Bar');
        $user->setFirstname('A Foo Bar');
        $user->setLastename('19.99');

        $dm = $this->get('doctrine_mongodb')->getManager();
        $dm->persist($user);
        $dm->flush();

        return new Response('Created product id '.$user->getId());
    }


    /**
     * @Route("/show", name="showProduct")
     * @return Response
     */
  /*  public function showAction()
    {
        $repository = $this->get('doctrine_mongodb')
            ->getManager()
            ->getRepository('AppBundle:Product');
        $products = $repository->findAll();
        if (!$products) {
            throw $this->createNotFoundException('No product found for id ');
        }
        return new Response(json_encode($products));

    }*/






    /**
     * @Rest\Get(
     *   path = "products"
     * )
     */
 /*  public function getAllLeagueAction()
    {
        $repository = $this->get('doctrine_mongodb')
            ->getManager()
            ->getRepository('AppBundle:Product');
        $leagues = $repository->findAll();
        //$leagues = $this->ProductRepository->findAll();

        $i = 0;
        $result = [];
        foreach ($leagues as $league) {
            $result[$i]['id'] = $league->getId();
            $result[$i]['name'] = $league->getName();
            $result[$i]['pice'] = $league->getPrice();

            $i++;
        }

        return new Response(json_encode($result));
    }*/



}


