<?php
/**
 * Created by PhpStorm.
 * User: safa
 * Date: 14/08/2018
 * Time: 9:36 PM
 */

namespace AppBundle\Controller;

use FOS\RestBundle\Controller\Annotations as Rest;
use AppBundle\Document\Utilisateur;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use FOS\RestBundle\Controller\FOSRestController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use FOS\RestBundle\Controller\Annotations\View;
use AppBundle\Document\Repository\UtilisateurRepository;


class UtilisateurController extends Controller
{
    /**
     * @Rest\Post(
     *   path = "Users/"
     * )
     */
    public function RegisterUser(Request $request)
    {
        $user = new Utilisateur();
        $user->setUsername($request->request->get('username'));
        $user->setPassword($request->request->get('password'));
        $user->setEmail($request->request->get('email'));
        $user->setFirstname($request->request->get('firstname'));
        $user->setLastename($request->request->get('lastname'));

        $dm = $this->get('doctrine_mongodb')->getManager();
        $dm->persist($user);
        $dm->flush();
        return new Response('League ID:'.json_encode($user->getId()));
    }







    /**
     * @param string $property
     * @param string $data
     *
     * @Route("/{property}/{data}")
     */
    public function loginAction($username, $password)
    {
        $repository = $this->get('doctrine_mongodb')
            ->getManager()
           ->getRepository('AppBundle:Utilisateur');
        $user = $repository  ->find($username, $password);
        $user = $this->UtilistauerRepository->find($username, $password);
        if (!$user instanceof Utilisateur) {
            throw new NotFoundHttpException(
                sprintf('user with %s [%s] cannot be found.', $username, $password)
            );
        }
}
}