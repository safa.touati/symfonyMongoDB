<?php

/*
 * This file is part of the FOSUserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace AppBundle\Controller;

use AppBundle\Document\User;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\UserBundle\Event\FilterUserResponseEvent;
use FOS\UserBundle\Event\FormEvent;
use FOS\UserBundle\Event\GetResponseUserEvent;
use FOS\UserBundle\Form\Factory\FactoryInterface;
use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Model\UserInterface;
use FOS\UserBundle\Model\UserManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Core\Encoder\BCryptPasswordEncoder;

/**
 * Controller managing the registration.
 *
 * @author Thibault Duplessis <thibault.duplessis@gmail.com>
 * @author Christophe Coevoet <stof@notk.org>
 */
class UserController extends Controller
{
    use \AppBundle\Helper\ControllerHelper;
    private $eventDispatcher;

    private $userManager;
    private $tokenStorage;

    public function __construct(EventDispatcherInterface $eventDispatcher,  UserManagerInterface $userManager, TokenStorageInterface $tokenStorage)
    {
        $this->eventDispatcher = $eventDispatcher;
        $this->userManager = $userManager;
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @Rest\Post(
     *    path = "signup",
     * )
     */
    public function SignUpAction(Request $request)
    {
        $user = $this->userManager->createUser();
        
        $user->setUsername($request->request->get('username'));

        $password=$request->request->get('password');
        if(($password!=null)and($password!="")) {
            $options = array('cost' => 13);
            //crypt the new password
            $passwordHash = password_hash($password, PASSWORD_BCRYPT, $options);

        }else{
            return new JsonResponse("invalid password ");
        }
        $user->setPassword($passwordHash);
        $user->setEmail($request->request->get('email'));
        $user->setFirstname($request->request->get('firstname'));
        $user->setLastename($request->request->get('lastname'));

       /* $dm = $this->get('doctrine_mongodb')->getManager();
        $dm->persist($user);
        $dm->flush();
        return new Response('League ID:'.json_encode($user->getId()));*/



        $user->setEnabled(true);

        $this->userManager->updateUser($user);

        return new Response('League ID:'.json_encode($user->getId()));
    }


    /**
     * @Rest\Post(
     *    path = "login",
     * )
     */
    public function loginAction(Request $request)
    {
        $username=$request->request->get('username');
        $password=($request->request->get('password'));

        $user  = $this->get('doctrine_mongodb')
            ->getManager()
            ->getRepository('AppBundle:User')
            ->findOneBy(['username' => $username]);
        $oldPassword=$user->getPassword();

        if(($password!=null)and($password!="")) {
            $options = array('cost' => 13);
            //crypt the new password
            $passwordHash = password_hash($password, PASSWORD_BCRYPT, $options);

        }
        $encoder = new BCryptPasswordEncoder(13);
        //verify that the password entered by the user is correct
        $valid = $encoder->isPasswordValid($oldPassword, $password, '');

        $token = $this->getToken($user);
        $response = new Response($this->serialize(['token' => $token]), Response::HTTP_OK);

        return $this->setBaseHeaders($response);

    }


    public function getToken(User $user)
    {
        return $this->container->get('lexik_jwt_authentication.encoder')
            ->encode([
                'username' => $user->getUsername(),
                'exp' => $this->getTokenExpiryDateTime(),
            ]);
    }

    /**
     * Returns token expiration datetime.
     *
     * @return string Unixtmestamp
     */
    private function getTokenExpiryDateTime()
    {
        $tokenTtl = $this->container->getParameter('lexik_jwt_authentication.token_ttl');
        $now = new \DateTime();
        $now->add(new \DateInterval('PT'.$tokenTtl.'S'));

        return $now->format('U');


}

}
