<?php
/**
 * Created by PhpStorm.
 * User: safa
 * Date: 27/08/2018
 * Time: 11:04 AM
 */

namespace AppBundle\Controller;

use AppBundle\Document\Formulaire;
use AppBundle\Document\Question;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use FOS\RestBundle\Controller\FOSRestController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use FOS\RestBundle\Controller\Annotations\View;

class QuestionController extends Controller
{
    /**
     * @Rest\Post(
     *   path = "form/question/{id}"
     * )
     */
    public function createQeustionAction(Request $request, $id)
    {
        $formulaire = $this->get('doctrine_mongodb')
            ->getRepository('AppBundle:Formulaire')
            ->find($id);

        if (!$formulaire) {
            throw $this->createNotFoundException('No form found for id '.$id);
        }


        $question = new Question();
        $question->setTitle($request->request->get('title'));
        $question->setCondition($request->request->get('condition'));
        $question->setQuestionType($request->request->get('questionType'));
        $question->setFormId($formulaire);

        $dm = $this->get('doctrine_mongodb')->getManager();
        $dm->persist($question);
        $dm->flush();

        return new Response('question ID:'.json_encode($question->getId()));
    }
    //update

    /**
     * @Rest\Post(
     *   path = "form/question/update/{id}"
     * )
     */
    public function updateQuestionForm(Request $request, $id)
    {
        $question = $this->get('doctrine_mongodb')
            ->getRepository('AppBundle:Question')
            ->find($id);

        if (!$question) {
            throw $this->createNotFoundException('No question found ');
        }

        $question->setTitle($request->request->get('title'));
        $question->setCondition($request->request->get('condition'));
        $question->setQuestionType($request->request->get('questionType'));

        $dm = $this->get('doctrine_mongodb')->getManager();
        $dm->persist($question);
        $dm->flush();

        return new Response('question ID:'.json_encode($question->getId()));
    }

    /**
     * @Rest\Delete(
     *   path = "form/question/delete/{id}"
     * )
     */
    public function deleteQuestion($id)
    {
        $question = $this->get('doctrine_mongodb')
            ->getRepository('AppBundle:Question')
            ->find($id);

        if (!$question) {
            throw $this->createNotFoundException('No question found ');
        }
        $dm = $this->get('doctrine_mongodb')->getManager();
        $dm->remove($question);
        $dm->flush();

        return new Response('question ID:'.$id);
    }

    /**
     * @Rest\Get(
     *   path = "form/question/liste/{id}"
     * )
     */
    public function getAllQuestAction($id)
    {
        $formulaire = $this->get('doctrine_mongodb')
            ->getRepository('AppBundle:Formulaire')
            ->find($id);

        if (!$formulaire) {
            throw $this->createNotFoundException('No user found for id '.$id);
        }

        $i = 0;
        $result = [];
        $questions = $formulaire->getQuestions();
        foreach ($questions as $question) {
            $result[$i]['title'] = (string)$question->getTitle();
            $result[$i]['condition'] = (string)$question->getCondition();
            $result[$i]['questionType'] = $question->getQuestionType();


            $i++;
        }

        return new Response(json_encode($result));
    }


    /**
     * @Rest\Get(
     *   path = "form/question/one/{id}"
     * )
     */

    public function getOneQuestAction($id)
    {
        $question = $this->get('doctrine_mongodb')
            ->getRepository('AppBundle:Question')
            ->find($id);

        if (!$question) {
            throw $this->createNotFoundException('No Forms found ');
        }

        $result['title'] = (string)$question->getTitle();
        $result['condition'] = (string)$question->getCondition();
        $result['questionType'] =(string) $question->getQuestionType();


        return new Response(json_encode($result));
    }

}