<?php
/**
 * Created by PhpStorm.
 * User: safa
 * Date: 17/08/2018
 * Time: 1:22 PM
 */

namespace AppBundle\Controller;


use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;


class WelcomeController extends Controller
{
    use \AppBundle\Helper\ControllerHelper;
    /**
     * @Rest\Get(
     *    path = "/api/welcome",
     * )
     */

    public function welcomeAction(Request $request)
    {
        $response = new Response($this->serialize('Hello user.'), Response::HTTP_OK);

        return $this->setBaseHeaders($response);
    }
}