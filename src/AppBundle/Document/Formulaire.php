<?php
/**
 * Created by PhpStorm.
 * User: safa
 * Date: 16/08/2018
 * Time: 3:19 PM
 */

namespace AppBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * @MongoDB\Document
 */
/**
 * @MongoDB\Document(repositoryClass="AppBundle\Document\Repository\FormulaireRepository")
 */
class Formulaire
{
    /**
     * @MongoDB\Id
     */
    protected $id;
    /**
     * @MongoDB\Field(type="string")
     */
    protected $title;
    /**
     * @MongoDB\Field(type="string")
     */
    protected $formDescription;
    /**
     * @MongoDB\Field(type="date")
     */
    protected $creationDate;
    /**
     * @MongoDB\Field(type="date")
     */
    protected $lastModifDate;

    /**
     * @MongoDB\ReferenceOne(targetDocument="User", inversedBy="formulaires")
     */
    private $user;
    /**
     * @MongoDB\ReferenceMany(targetDocument="Question", mappedBy="formulaire")
     */
    private $questions;
    public function __construct()
    {
        $this->questions = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return $this
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * Get title
     *
     * @return string $title
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set formDescription
     *
     * @param string $formDescription
     * @return $this
     */
    public function setFormDescription($formDescription)
    {
        $this->formDescription = $formDescription;
        return $this;
    }

    /**
     * Get formDescription
     *
     * @return string $formDescription
     */
    public function getFormDescription()
    {
        return $this->formDescription;
    }
    /**
     * Set creationDate
     *
     * @param date $creationDate
     * @return $this
     */
    public function setCreationDate($creationDate)
    {
        $this->creationDate = $creationDate;
        return $this;
    }

    /**
     * Get creationDate
     *
     * @return date $creationDate
     */
    public function getCreationDate()
    {
        return $this->creationDate;
    }
    /**
     * Set lastModifDate
     *
     * @param date $lastModifDate
     * @return $this
     */
    public function setLastModifDate($lastModifDate)
    {
        $this->lastModifDate = $lastModifDate;
        return $this;
    }

    /**
     * Get lastModifDate
     *
     * @return date $lastModifDate
     */
    public function getLastModifDate()
    {
        return $this->lastModifDate;
    }

    /**
     * Set user
     *
     * @param AppBundle\Document\User $user
     * @return $this
     */
    public function setUser(User $user)
    {
        $this->user = $user;
        return $this;
    }

    /**
     * Get user
     *
     * @return AppBundle\Document\User $user
     */
    public function getUser()
    {
        return $this->user;
    }




    /**
     * Add question
     *
     * @param AppBundle\Document\Question $question
     */
    public function addQuestion(Question $question)
    {
        $this->questions[] = $question;
    }

    /**
     * Remove question
     *
     * @param AppBundle\Document\Question $question
     */
    public function removeQuestion(Question $question)
    {
        $this->questions->removeElement($question);
    }

    /**
     * Get questions
     *
     * @return \Doctrine\Common\Collections\Collection $questions
     */
    public function getQuestions()
    {
        return $this->questions;
    }
}
