<?php
/**
 * Created by PhpStorm.
 * User: safa
 * Date: 27/08/2018
 * Time: 10:08 AM
 */

namespace AppBundle\Document;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * @MongoDB\Document
 */
/**
 * @MongoDB\Document(repositoryClass="AppBundle\Document\Repository\QuestionRepository")
 */

class Question
{
    /**
     * @MongoDB\Id
     */
    protected $id;
    /**
     * @MongoDB\Field(type="string")
     */
    protected $title;
    /**
     * @MongoDB\Field(type="string")
     */
    protected $condition;

    /**
     * @MongoDB\Field(type="string")
     */
    protected $questionType;
    /**
     * @MongoDB\ReferenceOne(targetDocument="Formulaire", inversedBy="questions")
     */
    private $form_id;


    /**
     * Get id
     *
     * @return id $id
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return $this
     */
    public function setTitle($title)
    {
        $this->title = $title;
        return $this;
    }

    /**
     * Get title
     *
     * @return string $title
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set condition
     *
     * @param string $condition
     * @return $this
     */
    public function setCondition($condition)
    {
        $this->condition = $condition;
        return $this;
    }

    /**
     * Get condition
     *
     * @return string $condition
     */
    public function getCondition()
    {
        return $this->condition;
    }

    /**
     * Set questionType
     *
     * @param string $questionType
     * @return $this
     */
    public function setQuestionType($questionType)
    {
        $this->questionType = $questionType;
        return $this;
    }

    /**
     * Get questionType
     *
     * @return string $questionType
     */
    public function getQuestionType()
    {
        return $this->questionType;
    }

    /**
     * Set formId
     *
     * @param AppBundle\Document\Formulaire $formId
     * @return $this
     */
    public function setFormId(Formulaire $formId)
    {
        $this->form_id = $formId;
        return $this;
    }

    /**
     * Get formId
     *
     * @return AppBundle\Document\Formulaire $formId
     */
    public function getFormId()
    {
        return $this->form_id;
    }


}
